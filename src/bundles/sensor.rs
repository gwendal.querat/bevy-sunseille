use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::components::SensorComponentTag;
use crate::{Transform, TransformBundle};

#[derive(Bundle)]
pub struct SensorBundle {
    #[bundle]
    pub transform: TransformBundle,
    pub collider: Collider,
    pub sensor: Sensor,
    pub active_events: ActiveEvents,
    pub collider_mass_properties: ColliderMassProperties,
    pub sensor_component: SensorComponentTag,
    pub colliding_entities: CollidingEntities,
}

impl Default for SensorBundle {
    fn default() -> Self {
        Self {
            collider: Collider::ball(25.),
            transform: TransformBundle::from_transform(Transform::from_xyz(0., 0., 0.)),
            sensor: Sensor,
            // active_collision_types: ActiveCollisionTypes::STATIC_STATIC,
            active_events: ActiveEvents::COLLISION_EVENTS,
            // If I don't add this, it REALLY impacts the speed of the entity's parent
            collider_mass_properties: ColliderMassProperties::Density(0.0),
            sensor_component: SensorComponentTag,
            colliding_entities: CollidingEntities::default(),
        }
    }
}
