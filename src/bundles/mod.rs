use bevy::prelude::*;

use crate::components::CornerPopupTag;
use crate::SpriteBundle;

pub mod sensor;

#[derive(Bundle)]
pub struct CornerPopupBundle {
    #[bundle]
    pub sprite_bundle: SpriteBundle,
    pub interaction_icon: CornerPopupTag,
}
