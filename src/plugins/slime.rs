use benimator::FrameRate;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::bundles::sensor::SensorBundle;
use crate::components::animations::Animation;
use crate::components::animations::AnimationState;
use crate::components::character::CharacterComponent;
use crate::components::PlayerInteractableTag;
use crate::constants::animations::{
    SLIME_ANIMATION_ATTACKING, SLIME_ANIMATION_IDLE, SLIME_ANIMATION_RUNNING,
};
use crate::events::EvtChangeAnimationRequest;
use crate::resources::AnimationsDict;
use crate::EvtAnimationEnded;

#[derive(Default, Component)]
pub struct SlimeComponent;

fn spawn_slime(
    mut commands: Commands,
    mut textures: ResMut<Assets<TextureAtlas>>,
    mut animations: ResMut<AnimationsDict>,
    asset_server: Res<AssetServer>,
) {
    let texture = TextureAtlas::from_grid(
        asset_server.load("mystic_woods/sprites/characters/slime.png"),
        Vec2::new(32.0, 32.0),
        7,
        5,
        None,
        None,
    );

    // TODO use constant string identifiers / Enum for animation keys
    animations.entries.insert(
        SLIME_ANIMATION_IDLE.into(),
        Animation(
            benimator::Animation::from_indices([0, 1, 2, 3], FrameRate::from_fps(12.)).repeat(),
        ),
    );
    animations.entries.insert(
        SLIME_ANIMATION_RUNNING.into(),
        Animation(
            benimator::Animation::from_indices([7, 8, 9, 10, 11, 12], FrameRate::from_fps(12.))
                .once(),
        ),
    );
    animations.entries.insert(
        SLIME_ANIMATION_ATTACKING.into(),
        Animation(
            benimator::Animation::from_indices(
                [14, 15, 16, 17, 18, 19, 20],
                FrameRate::from_fps(12.),
            )
            .once(),
        ),
    );

    commands
        .spawn((
            (
                SpriteSheetBundle {
                    texture_atlas: textures.add(texture),
                    visibility: Visibility::Visible,
                    transform: Transform {
                        translation: Vec3::new(210., 100., 998.),
                        ..default()
                    },
                    ..Default::default()
                },
                animations
                    .entries
                    .get(SLIME_ANIMATION_IDLE)
                    .expect("you fucked up a string")
                    .clone(),
                SlimeComponent::default(),
                AnimationState::default(),
                CharacterComponent::default(),
                RigidBody::Dynamic,
                AdditionalMassProperties::Mass(0.5),
                GravityScale(0.),
                Collider::ball(8.),
                LockedAxes::ROTATION_LOCKED,
                Ccd::enabled(),
                PlayerInteractableTag,
                Restitution {
                    coefficient: 2.,
                    ..default()
                },
                Friction {
                    coefficient: -0.01,
                    ..default()
                },
                Damping {
                    linear_damping: -0.1,
                    ..default()
                },
            ),
            Name::from("Slime"),
        ))
        .with_children(|child_builder| {
            child_builder.spawn(SensorBundle::default());
        });
}

pub fn test_slime_animation_with_keyboard(
    mut evt_writer: EventWriter<EvtChangeAnimationRequest>,
    keyboard: Res<Input<KeyCode>>,
    mut query: Query<Entity, With<SlimeComponent>>,
) {
    for entity in query.iter_mut() {
        if keyboard.pressed(KeyCode::Numpad1) {
            info!("change anim to idle");
            evt_writer.send(EvtChangeAnimationRequest {
                entity,
                animation: SLIME_ANIMATION_IDLE.into(),
            });
        }
        if keyboard.pressed(KeyCode::Numpad2) {
            info!("change anim to running");
            evt_writer.send(EvtChangeAnimationRequest {
                entity,
                animation: SLIME_ANIMATION_RUNNING.into(),
            });
        }
        if keyboard.pressed(KeyCode::Numpad3) {
            info!("change anim to attacking");
            evt_writer.send(EvtChangeAnimationRequest {
                entity,
                animation: SLIME_ANIMATION_ATTACKING.into(),
            });
        }
    }
}

pub fn make_slime_idle_on_animation_end(
    mut evt_reader: EventReader<EvtAnimationEnded>,
    mut evt_writer: EventWriter<EvtChangeAnimationRequest>,
    mut query_slime: Query<(Entity, &mut AnimationState), With<SlimeComponent>>,
) {
    for evt in evt_reader.iter() {
        if let Ok((slime_entity, mut animation_state)) = query_slime.get_mut(evt.entity_id) {
            evt_writer.send(EvtChangeAnimationRequest {
                entity: slime_entity,
                animation: SLIME_ANIMATION_IDLE.into(),
            });
            animation_state.state.reset();
        }
    }
}

pub struct SlimePlugin;

impl Plugin for SlimePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_slime);
    }
}
