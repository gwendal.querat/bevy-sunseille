use benimator::FrameRate;
use bevy::prelude::*;
use bevy::sprite::Anchor;
use bevy_rapier2d::prelude::*;

use crate::components::animations::*;
use crate::components::attack_cool_down::AttackCooldown;
use crate::components::character::*;
use crate::constants::animations::{
    PLAYER_ANIMATION_ATTACKING, PLAYER_ANIMATION_IDLE, PLAYER_ANIMATION_RUNNING,
};
use crate::events::{
    AttackCooldownEnded, EvtAnimationEnded, EvtChangeAnimationRequest, EvtMeleeAttackRequested,
    EvtMeleeAttackStarted,
};
use crate::resources::AnimationsDict;
use crate::wip::inventory::{Inventory, InventorySlot, Item, ItemQuantity};

pub struct PlayerPlugin;

#[derive(Component, Debug, Reflect, Default)]
pub struct PlayerComponent {
    pub inventory: Inventory,
}

#[derive(Component, Debug, Reflect, Default)]
pub struct PlayerSensor;

pub struct PlayerCreation {
    pub player_id: Entity,
}

pub fn init_inventory(commands: &mut Commands, asset_server: Res<AssetServer>) -> Inventory {
    info!("init_inventory");

    let slot_entity = commands
        .spawn(Item {
            id: "TODO".to_string(),
            item_quantity: ItemQuantity::One,
            icon: asset_server.load("cat.png"),
        })
        .id();

    Inventory {
        content: Vec::from([
            Vec::from([
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Entity(slot_entity),
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
            ]),
            Vec::from([
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
            ]),
            Vec::from([
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
            ]),
            Vec::from([
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
                InventorySlot::Empty,
            ]),
        ]),
    }
}

pub fn tick_and_remove_attack_cooldown(
    mut query: Query<(Entity, &mut AttackCooldown)>,
    mut event_writer: EventWriter<AttackCooldownEnded>,
    mut commands: Commands,
    time: Res<Time>,
) {
    for (entity, mut attack_cooldown) in query.iter_mut() {
        attack_cooldown.timer.tick(time.delta());
        if attack_cooldown.timer.finished() {
            debug!("Removing cooldown");
            commands.entity(entity).remove::<AttackCooldown>();
            event_writer.send(AttackCooldownEnded(entity));
        }
    }
}

pub fn player_attack_scheduler(
    mut commands: Commands,
    mut try_melee_attack_events: EventReader<EvtMeleeAttackRequested>,
    mut melee_attack_started_events: EventWriter<EvtMeleeAttackStarted>,
    mut cooldown_ended_events: EventReader<AttackCooldownEnded>,
    mut animation_changed_writer: EventWriter<EvtChangeAnimationRequest>,
    mut query: Query<(&mut CharacterComponent, Option<&mut AttackCooldown>), With<PlayerComponent>>,
) {
    for event in try_melee_attack_events.iter() {
        let entity_id = event.0;
        let mut entity = commands.entity(entity_id);

        if let Ok((mut character, mut cooldown)) = query.get_mut(entity_id) {
            if cooldown.as_mut().is_none() {
                entity.insert(AttackCooldown::new(0.25));
                melee_attack_started_events.send(EvtMeleeAttackStarted(entity_id));
                character.current_action = Action::Attacking;
                animation_changed_writer.send(EvtChangeAnimationRequest {
                    entity: entity_id,
                    animation: PLAYER_ANIMATION_ATTACKING.to_string(),
                });
            };
            continue;
        }
        error!("EvtTryMeleeAttack sent with invalid entity id (no PlayerComponent found)");
    }

    for event in cooldown_ended_events.iter() {
        let entity_id = event.0;
        if let Ok((mut character, _cooldown)) = query.get_mut(entity_id) {
            character.current_action = Action::Idle;
            continue;
        }
        error!("EvtMeleeAttackEnded sent with invalid entity id (no PlayerComponent found)");
    }
}

pub fn player_interaction(
    mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    click_input: Res<Input<MouseButton>>,
    mut attack_writer: EventWriter<EvtMeleeAttackRequested>,
    mut animation_change_writer: EventWriter<EvtChangeAnimationRequest>,
    mut query: Query<(Entity, &mut CharacterComponent), With<PlayerComponent>>,
    time: Res<Time>,
) {
    let mut impulse_value = 15000. * time.delta_seconds();

    for (entity, mut character) in query.iter_mut() {
        let mut impulse = Vec2::ZERO;
        let mut entity = commands.entity(entity);

        if click_input.just_pressed(MouseButton::Left) {
            attack_writer.send(EvtMeleeAttackRequested(entity.id()));
        }

        // Can't move while attacking !
        if character.current_action == Action::Attacking {
            impulse_value = 0.;
            continue;
        }

        // FIXME Hardware keycodes for intl (Eg. Azerty keyboards)
        let move_pressed = [KeyCode::W, KeyCode::A, KeyCode::S, KeyCode::D]
            .iter()
            .any(|key| keyboard_input.pressed(*key));

        if move_pressed && character.current_action != Action::Running {
            trace!("move pressed");
            animation_change_writer.send(EvtChangeAnimationRequest {
                entity: entity.id(),
                animation: PLAYER_ANIMATION_RUNNING.to_string(),
            });
            character.current_action = Action::Running;
            continue;
        }
        if keyboard_input.pressed(KeyCode::W) {
            impulse.y += impulse_value;
            // TODO with 4 way sprite
            // character.direction = crate::character::Direction::_Up;
        }
        if keyboard_input.pressed(KeyCode::S) {
            // TODO with 4 way sprite
            // character.direction = crate::character::Direction::_Down;
            impulse.y -= impulse_value;
        }
        if keyboard_input.pressed(KeyCode::D) {
            character.direction = crate::components::character::Direction::Right;
            impulse.x += impulse_value;
        }
        if keyboard_input.pressed(KeyCode::A) {
            character.direction = crate::components::character::Direction::Left;
            impulse.x -= impulse_value;
        }

        entity.insert(ExternalImpulse {
            impulse,
            ..default()
        });

        // Can't remove this chunk by setting the animation to IDLE after the running
        // animation ends : the "repeat" flag means it does not end !
        if !move_pressed && character.current_action != Action::Idle {
            trace!("idle");
            animation_change_writer.send(EvtChangeAnimationRequest {
                entity: entity.id(),
                animation: PLAYER_ANIMATION_IDLE.to_string(),
            });
            character.current_action = Action::Idle;
        }
    }
}

pub fn startup_create_player(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut textures: ResMut<Assets<TextureAtlas>>,
    mut animations: ResMut<AnimationsDict>,
    mut player_creation_evt_writer: EventWriter<PlayerCreation>,
) {
    animations.entries.insert(
        PLAYER_ANIMATION_IDLE.to_string(),
        Animation(
            benimator::Animation::from_indices([0, 1, 2, 3, 4, 5], FrameRate::from_fps(12.))
                .repeat(),
        ),
    );
    animations.entries.insert(
        PLAYER_ANIMATION_RUNNING.to_string(),
        Animation(
            benimator::Animation::from_indices([6, 7, 8, 9, 10, 11], FrameRate::from_fps(12.))
                .repeat(),
        ),
    );
    animations.entries.insert(
        PLAYER_ANIMATION_ATTACKING.to_string(),
        Animation(
            benimator::Animation::from_indices([12, 13, 14, 15], FrameRate::from_fps(12.)).once(),
        ),
    );

    let texture = TextureAtlas::from_grid(
        asset_server.load("mystic_woods/sprites/characters/player.png"),
        Vec2::new(48.0, 48.0),
        6,
        5,
        None,
        None,
    );

    let player_inventory = init_inventory(&mut commands, asset_server);

    let player_id = commands
        .spawn((
            SpriteSheetBundle {
                texture_atlas: textures.add(texture),
                visibility: Visibility::Visible,
                transform: Transform {
                    translation: Vec3::new(200., 100., 998.),
                    ..default()
                },
                sprite: TextureAtlasSprite {
                    anchor: Anchor::Custom(Vec2::new(0., -0.15)),
                    ..default()
                },
                ..Default::default()
            },
            AnimationState::default(),
            PlayerComponent {
                inventory: player_inventory,
            },
            CharacterComponent::default(),
            RigidBody::Dynamic,
            AdditionalMassProperties::Mass(0.),
            GravityScale(0.),
            Collider::capsule_y(6., 7.0),
            LockedAxes::ROTATION_LOCKED,
            Ccd::enabled(),
            animations
                .entries
                .get(&PLAYER_ANIMATION_IDLE.to_string())
                .unwrap()
                .clone(),
            Damping {
                linear_damping: 100.,
                ..default()
            },
            Name::from("Player"),
        ))
        .id();
    info!("Player's ID: {:?}", player_id);
    player_creation_evt_writer.send(PlayerCreation { player_id });
}

pub fn player_animation_reset_to_idle(
    mut query: Query<(Entity, &mut TextureAtlasSprite, &mut AnimationState), With<PlayerComponent>>,
    mut event_reader: EventReader<EvtAnimationEnded>,
    mut event_writer: EventWriter<EvtChangeAnimationRequest>,
) {
    // If player's animation ended, set it to idle
    // (used to handle an attack animation ending)
    for evt in event_reader.iter() {
        if let Ok((entity_id, _sprite, _animation_state)) = query.get_mut(evt.entity_id) {
            event_writer.send(EvtChangeAnimationRequest {
                entity: entity_id,
                animation: PLAYER_ANIMATION_IDLE.to_string(),
            });
        }
    }
}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(startup_create_player)
            .add_event::<EvtMeleeAttackRequested>()
            .add_event::<EvtMeleeAttackStarted>()
            .add_event::<AttackCooldownEnded>()
            .add_event::<PlayerCreation>()
            .register_type::<PlayerComponent>()
            .register_type::<Action>()
            .register_type::<Direction>();
    }
}
