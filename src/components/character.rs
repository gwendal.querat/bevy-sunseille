use bevy::prelude::*;

#[derive(Component, Reflect, Debug, Copy, Clone)]
pub enum Direction {
    _Up,
    _Down,
    Left,
    Right,
}

#[derive(Debug, Reflect, Copy, Clone, PartialEq, Eq)]
pub enum Action {
    Running,
    Idle,
    Attacking,
}

#[derive(Component, Reflect, Debug, Copy, Clone)]
pub struct CharacterComponent {
    pub direction: Direction,
    pub current_action: Action,
}

impl Default for CharacterComponent {
    fn default() -> Self {
        CharacterComponent {
            direction: Direction::Right,
            current_action: Action::Idle,
        }
    }
}
