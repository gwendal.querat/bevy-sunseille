use bevy::{prelude::*, reflect::TypeUuid};

#[derive(Default, Component)]
pub struct AnimationState {
    pub state: benimator::State,
    pub evt_ended_fired: bool,
}

// Animation asset
#[derive(TypeUuid, Deref, DerefMut, Component, Clone)]
#[uuid = "e6071abd-2150-429a-bdf7-9fbb377c62a9"]
pub struct Animation(pub benimator::Animation);
