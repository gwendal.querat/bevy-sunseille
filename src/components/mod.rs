use bevy::prelude::*;

pub mod animations;
pub mod attack_cool_down;
pub mod character;

#[derive(Component)]
pub struct CornerPopupTag;

#[derive(Component)]
pub struct PlayerInteractableTag;

#[derive(Component)]
pub struct SensorComponentTag;
