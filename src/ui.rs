use bevy::prelude::*;
use kayak_ui::prelude::{widgets::*, *};

use crate::ui_components::fps_counter::setup_fps_counter_ui;
use crate::ui_components::inventory::setup_inventory_ui;
use crate::ui_components::item_slot::setup_item_slot_ui;
use crate::ui_components::menu::setup_menu_ui;
use crate::ui_components::npc_dialog::setup_npc_dialog_ui_bundle;
use crate::{setup_root_ui_bundle, RootUiBundle};

#[derive(Component)]
pub struct MainCamera;

pub fn startup_kayak(
    mut commands: Commands,
    mut font_mapping: ResMut<FontMapping>,
    asset_server: Res<AssetServer>,
) {
    let camera_entity = commands
        .spawn((Camera2dBundle::default(), CameraUIKayak, MainCamera))
        .id();

    font_mapping.set_default(asset_server.load("fonts/roboto.kayak_font"));

    let mut widget_context = KayakRootContext::new(camera_entity);

    widget_context.add_plugin(KayakWidgetsContextPlugin);

    // Setup all of the application's UI Components
    setup_root_ui_bundle(&mut widget_context);
    setup_npc_dialog_ui_bundle(&mut widget_context);
    setup_fps_counter_ui(&mut widget_context);
    setup_menu_ui(&mut widget_context);
    setup_inventory_ui(&mut widget_context);
    setup_item_slot_ui(&mut widget_context);

    let parent_id = None;
    // The rsx! macro expects a parent_id, a widget_context from the user.
    // It also expects `Commands` from bevy.
    // This can be a little weird at first.
    // See the rsx! docs for more info!
    rsx! {
        <KayakAppBundle>
            <RootUiBundle/>
        </KayakAppBundle>
    };

    // needed from latest versions. At the bottom of the function to avoid use
    // of moved value in the rsx! macro above.
    commands.spawn((widget_context, EventDispatcher::default()));
}
