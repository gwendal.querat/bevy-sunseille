use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

pub fn resume_rapier(mut commands: Commands, res: Res<RapierConfiguration>) {
    info!("RESUMING Rapier2D ...");
    commands.insert_resource(RapierConfiguration {
        physics_pipeline_active: true,
        query_pipeline_active: true,
        ..*res
    })
}

pub fn pause_rapier(mut commands: Commands, res: Res<RapierConfiguration>) {
    info!("PAUSING Rapier2D ...");
    commands.insert_resource(RapierConfiguration {
        physics_pipeline_active: false,
        query_pipeline_active: false,
        ..*res
    })
}
