use crate::components::SensorComponentTag;
use crate::events::EvtEnteredSensorRange;
use crate::EvtExitedSensorRange;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

fn _which_is_sensor_and_which_is_other(
    e1: Entity,
    e2: Entity,
    query_sensor: &Query<&Parent, With<SensorComponentTag>>,
) -> (Entity, Entity) {
    if let Ok(parent) = query_sensor.get(e1) {
        return (parent.get(), e2);
    }
    if let Ok(parent) = query_sensor.get(e2) {
        return (parent.get(), e1);
    }
    panic!("_get_sensor_entity should have been sent one entity with a SensorComponent. That entity also needs a parent");
}

pub fn sensor_system(
    query_sensor: Query<&Parent, With<SensorComponentTag>>,
    mut event_writer_entering: EventWriter<EvtEnteredSensorRange>,
    mut event_writer_exiting: EventWriter<EvtExitedSensorRange>,
    mut event_reader: EventReader<CollisionEvent>,
) {
    for collision_event in event_reader.iter() {
        let e1;
        let e2;
        let is_collision_starting;

        match collision_event {
            CollisionEvent::Started(ee1, ee2, _) => {
                is_collision_starting = true;
                e1 = ee1;
                e2 = ee2;
                trace!("{:?} collides with {:?}", e1, e2);
            }
            CollisionEvent::Stopped(ee1, ee2, _) => {
                is_collision_starting = false;
                e1 = ee1;
                e2 = ee2;
                trace!("{:?} STOPS colliding with {:?}", e1, e2);
            }
        };
        let (sensor_entity, other_entity) =
            _which_is_sensor_and_which_is_other(*e1, *e2, &query_sensor);
        trace!(
            "  - Parent of entity with SensorComponent is {:?}",
            sensor_entity
        );
        trace!("  - Colliding entity is {:?}", other_entity);
        match is_collision_starting {
            true => event_writer_entering.send(EvtEnteredSensorRange {
                entity_with_sensor_child: sensor_entity,
                entity_entering_sensor_range: other_entity,
            }),
            false => event_writer_exiting.send(EvtExitedSensorRange {
                entity_with_sensor_child: sensor_entity,
                entity_exiting_sensor_range: other_entity,
            }),
        };
    }
}

pub fn sensor_system_debug(
    mut event_reader_entering: EventReader<EvtEnteredSensorRange>,
    mut event_reader_exiting: EventReader<EvtExitedSensorRange>,
) {
    for event in event_reader_entering.iter() {
        trace!("EvtEnteredSensorRange fired {:?}", event)
    }
    for event in event_reader_exiting.iter() {
        trace!("EvtExitedSensorRange fired {:?}", event)
    }
}
