use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::bundles::CornerPopupBundle;
use crate::components::{CornerPopupTag, PlayerInteractableTag, SensorComponentTag};
use crate::constants::animations::SLIME_ANIMATION_ATTACKING;
use crate::events::EvtPlayerInteraction;
use crate::plugins::player::{PlayerComponent, PlayerSensor};
use crate::plugins::slime::SlimeComponent;
use crate::{EvtChangeAnimationRequest, EvtEnteredSensorRange, EvtExitedSensorRange, GameState};

#[derive(Bundle)]
pub struct PlayerSensorBundle {
    #[bundle]
    pub transform: TransformBundle,
    pub collider: Collider,
    pub sensor: Sensor,
    pub active_events: ActiveEvents,
    pub collider_mass_properties: ColliderMassProperties,
    pub player_sensor: PlayerSensor,
    pub colliding_entities: CollidingEntities,
}

impl Default for PlayerSensorBundle {
    fn default() -> Self {
        Self {
            collider: Collider::ball(25.),
            transform: TransformBundle::from_transform(Transform::from_xyz(0., 0., 0.)),
            sensor: Sensor,
            // active_collision_types: ActiveCollisionTypes::STATIC_STATIC,
            active_events: ActiveEvents::COLLISION_EVENTS,
            // If I don't add this, it REALLY impacts the speed of the entity's parent
            collider_mass_properties: ColliderMassProperties::Density(0.0),
            player_sensor: PlayerSensor,
            colliding_entities: CollidingEntities::default(),
        }
    }
}

pub fn highlight_interactable_on_player_proximity(
    mut commands: Commands,
    mut evt_reader_entering: EventReader<EvtEnteredSensorRange>,
    mut query_interactable: Query<Entity, With<PlayerInteractableTag>>,
    query_player: Query<With<PlayerComponent>>,
    asset_server: Res<AssetServer>,
) {
    for event in evt_reader_entering.iter() {
        if query_player
            .get(event.entity_entering_sensor_range)
            .is_err()
        {
            trace!("player not found");
            continue;
        }

        trace!("player found");
        if let Ok(entity_id) = query_interactable.get_mut(event.entity_with_sensor_child) {
            trace!("Addding a popup sprite to {:?}", entity_id);
            commands.entity(entity_id).with_children(|child_builder| {
                child_builder.spawn(CornerPopupBundle {
                    sprite_bundle: SpriteBundle {
                        texture: asset_server.load("cat.png"),
                        transform: Transform {
                            translation: Vec3::new(15., 15., 0.),
                            scale: Vec3::new(0.05, 0.05, 0.),
                            ..default()
                        },
                        ..default()
                    },
                    interaction_icon: CornerPopupTag,
                });
            });
        } else {
            error!(
                "Entity with sensor not found {:?} !",
                event.entity_with_sensor_child
            );
        }
    }
}

pub fn stop_highlighting_interactable_on_player_proximity(
    mut commands: Commands,
    mut evt_reader_exiting: EventReader<EvtExitedSensorRange>,
    query_popup: Query<Entity, With<CornerPopupTag>>,
    query_interactable: Query<(Entity, &Children), With<PlayerInteractableTag>>,
    query_player: Query<With<PlayerComponent>>,
) {
    for event in evt_reader_exiting.iter() {
        if query_player.get(event.entity_exiting_sensor_range).is_err() {
            continue;
        }

        if let Ok((parent_entity, children)) =
            query_interactable.get(event.entity_with_sensor_child)
        {
            trace!("Trying to find entity with CornerPopupTag");
            // TODO there has to be a better way than O(n²)
            for child in children.iter() {
                if let Ok(entity_id) = query_popup.get(*child) {
                    trace!("removing corner popup from entity {:?}", entity_id);
                    commands.entity(entity_id).despawn_recursive();
                    return;
                }
            }
            trace!(
                "Entity {:?} does not have CornerPopupTag / does not exists",
                parent_entity
            );
        }
    }
}

pub fn detect_player_interaction(
    keyboard: Res<Input<KeyCode>>,
    query_player: Query<Entity, With<PlayerComponent>>,
    query_sensor: Query<(Entity, &Parent, &CollidingEntities), With<SensorComponentTag>>,
    mut evt_writer: EventWriter<EvtPlayerInteraction>,
) {
    // TODO keyboard bindings
    if !keyboard.just_pressed(KeyCode::E) {
        return;
    }
    trace!("Player trying to interact");
    for (sensor_entity, parent_of_sensor, colliding_entities) in query_sensor.iter() {
        let players_in_sensor_range: Vec<Entity> = colliding_entities
            .iter()
            .filter(|colliding_entity| query_player.get(*colliding_entity).is_ok())
            .collect();
        trace!(
            "  - PlayerInteractableTag in range for SensorComponentTag {:?}: {:?}",
            sensor_entity,
            Vec::from_iter(&players_in_sensor_range)
        );
        if players_in_sensor_range.is_empty() {
            trace!("  - Not near a PlayerInteractableTag, skipping");
            continue;
        }

        evt_writer.send(EvtPlayerInteraction {
            interactable_entity: parent_of_sensor.get(),
            player_entity: players_in_sensor_range[0],
        });
    }
}

pub fn animate_slime_on_interaction(
    mut event_reader: EventReader<EvtPlayerInteraction>,
    mut event_writer_animation: EventWriter<EvtChangeAnimationRequest>,
    mut event_writer_talk_to_npc: EventWriter<EvtTalkToNpc>,
    query_slime: Query<Entity, With<SlimeComponent>>,
) {
    for event in event_reader.iter() {
        if let Ok(entity) = query_slime.get(event.interactable_entity) {
            event_writer_animation.send(EvtChangeAnimationRequest {
                animation: SLIME_ANIMATION_ATTACKING.to_string(),
                entity,
            });
            event_writer_talk_to_npc.send(EvtTalkToNpc);
        }
    }
}

pub fn talk_to_npc(mut commands: Commands, mut event_reader: EventReader<EvtTalkToNpc>) {
    if event_reader.iter().count() > 0 {
        commands.insert_resource(NextState(Some(GameState::TalkingToNpc)));
        info!("Talking to npc -> commands.insert_resource(NextState(GameState::TalkingToNpc)");
    }
}

#[derive(Debug)]
pub struct EvtTalkToNpc;

pub struct EvtOpenNpcWindow;

pub struct TmpPlugin;

impl Plugin for TmpPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<EvtTalkToNpc>()
            .add_event::<EvtOpenNpcWindow>();
    }
}
