use bevy::prelude::*;

use crate::components::animations::Animation;
use crate::components::animations::AnimationState;
use crate::components::character::CharacterComponent;
use crate::events::EvtAnimationEnded;
use crate::events::EvtChangeAnimationRequest;
use crate::resources::AnimationsDict;

#[allow(clippy::type_complexity)]
pub fn character_animation(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(
        Entity,
        &mut AnimationState,
        &mut TextureAtlasSprite,
        &mut Animation,
        &CharacterComponent,
    )>,
    mut event_reader: EventReader<EvtChangeAnimationRequest>,
    mut event_writer: EventWriter<EvtAnimationEnded>,
    animations_dict: Res<AnimationsDict>,
) {
    // Reset entity's animation state if a new animation is requested (EvtChangeAnimationRequest)
    for event in event_reader.iter() {
        info!("Animation changed - {:?}", event);
        if let Ok((_, mut animation_state, _, _, _)) = query.get_mut(event.entity) {
            animation_state.state.reset();
            animation_state.evt_ended_fired = false;
            commands.entity(event.entity).insert(
                animations_dict
                    .entries
                    .get(&event.animation)
                    .unwrap_or_else(|| {
                        panic!("Invalid animation requested \"{}\" !", &event.animation)
                    })
                    .clone(),
            );
            continue;
        }
    }

    for (entity_id, mut animation_state, mut texture, animation, character) in query.iter_mut() {
        animation_state.state.update(&animation, time.delta());

        if animation_state.state.is_ended() && !animation_state.evt_ended_fired {
            event_writer.send(EvtAnimationEnded { entity_id })
        }

        // flip sprite depending on player direction
        match character.direction {
            crate::components::character::Direction::_Up => {
                todo!("4 way sprite (up)");
            }
            crate::components::character::Direction::_Down => {
                todo!("4 way sprite (down)");
            }
            crate::components::character::Direction::Left => texture.flip_x = true,
            crate::components::character::Direction::Right => texture.flip_x = false,
        };

        // Update the texture atlas
        texture.index = animation_state.state.frame_index();
    }
}
