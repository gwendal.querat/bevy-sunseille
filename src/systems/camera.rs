use bevy::input::mouse::MouseWheel;
use bevy::prelude::*;

use crate::plugins::player::PlayerComponent;
use crate::ui::MainCamera;

const CAMERA_MAX_ZOOM: f32 = 1.;
const CAMERA_MIN_ZOOM: f32 = 0.1;

#[allow(clippy::type_complexity)]
pub fn camera_translation_control(
    mut query_set: ParamSet<(
        Query<&Transform, With<PlayerComponent>>,
        Query<&mut Transform, With<MainCamera>>,
    )>,
) {
    let player_position = query_set
        .p0()
        .get_single_mut()
        .map(|player_transform: &Transform| player_transform.translation);

    let player_position = match player_position {
        Ok(position) => position,
        Err(err) => {
            error!("No player found: {:?}", err);
            error!("Could not update camera pos/zoom");
            return;
        }
    };

    query_set
        .p1()
        .get_single_mut()
        .map(|mut camera_transform| {
            // Keep the camera on top of the player
            camera_transform.translation = Vec3::new(
                player_position.x,
                player_position.y,
                camera_transform.translation.z, // Making sure to keep camera's z-level
            );
        })
        .unwrap_or_else(|err| error!("Could not update camera/projection: {:?}", err));
}

pub fn camera_zoom_control(
    mut mwheel: EventReader<MouseWheel>,
    mut camera: Query<&mut OrthographicProjection, With<MainCamera>>,
) {
    camera
        .get_single_mut()
        .map(|mut orthographic_projection| {
            for event in mwheel.iter() {
                if event.y <= 0. {
                    orthographic_projection.scale += 0.05;
                } else {
                    orthographic_projection.scale -= 0.05;
                }
            }
            orthographic_projection.scale = orthographic_projection
                .scale
                .clamp(CAMERA_MIN_ZOOM, CAMERA_MAX_ZOOM);
        })
        .unwrap_or_else(|err| error!("Could not update camera/projection: {:?}", err));
}

#[cfg(test)]
mod tests {
    use crate::plugins::player::PlayerComponent;
    use crate::systems::camera::{CAMERA_MAX_ZOOM, CAMERA_MIN_ZOOM};
    use crate::ui::MainCamera;
    use crate::wip::inventory::Inventory;
    use crate::{camera_translation_control, camera_zoom_control};
    use bevy::input::mouse::{MouseScrollUnit, MouseWheel};
    use bevy::prelude::*;

    #[test]
    fn test_camera_translation_control() {
        // Setup app
        let mut app = App::new();

        app.add_system(camera_translation_control);

        // Setup test entities
        let camera_id = app
            .world
            .spawn(Camera2dBundle::default())
            .insert(MainCamera)
            .id();

        let player_id = app
            .world
            .spawn(Transform::from_xyz(42., 42., 0.))
            .insert(PlayerComponent {
                inventory: Inventory::default(),
            })
            .id();

        // Run systems
        app.update();

        let player_position = app.world.get::<Transform>(player_id).unwrap().translation;
        let camera_position = app.world.get::<Transform>(camera_id).unwrap().translation;

        // truncate() avoids Z comparison since camera is at ~1000. VS player's 0.
        assert_eq!(camera_position.truncate(), player_position.truncate());
    }

    #[test]
    fn test_camera_zoom_control_backwards() {
        let mut app = App::new();
        app.add_event::<MouseWheel>();
        app.add_system(camera_zoom_control);

        let camera_id = app
            .world
            .spawn(Camera2dBundle::default())
            .insert(MainCamera)
            .id();

        app.world
            .resource_mut::<Events<MouseWheel>>()
            .send(MouseWheel {
                x: 0.0,
                y: 5.0, // no idea. but I'm only checkin >0 and <0 anyways
                unit: MouseScrollUnit::Line,
            });

        app.update();

        let camera_scale = app
            .world
            .get::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale;
        assert_eq!(camera_scale, 0.95);
    }

    #[test]
    fn test_camera_zoom_control_forwards() {
        let mut app = App::new();

        app.add_event::<MouseWheel>();
        app.add_system(camera_zoom_control);

        let camera_id = app
            .world
            .spawn(Camera2dBundle::default())
            .insert(MainCamera)
            .id();

        // custom scale because values are clamped, and it's a subject for another test
        app.world
            .get_mut::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale = 0.45;

        app.world
            .resource_mut::<Events<MouseWheel>>()
            .send(MouseWheel {
                x: 0.0,
                y: -5.0, // no idea. but I'm only checkin >0 and <0 anyways
                unit: MouseScrollUnit::Line,
            });

        // Run systems
        app.update();

        let camera_scale = app
            .world
            .get::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale;
        assert_eq!(camera_scale, 0.50);
    }

    #[test]
    fn test_camera_zoom_control_clamping_up() {
        let mut app = App::new();

        app.add_event::<MouseWheel>();
        app.add_system(camera_zoom_control);

        // Setup test entities
        let camera_id = app
            .world
            .spawn(Camera2dBundle::default())
            .insert(MainCamera)
            .id();

        // custom scale to trigger clamping
        app.world
            .get_mut::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale = 0.98;

        app.world
            .resource_mut::<Events<MouseWheel>>()
            .send(MouseWheel {
                x: 0.0,
                y: -5.0, // no idea. but I'm only checkin >0 and <0 anyways
                unit: MouseScrollUnit::Line,
            });

        // Run systems
        app.update();

        let camera_scale = app
            .world
            .get::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale;
        assert_eq!(camera_scale, CAMERA_MAX_ZOOM);
    }

    #[test]
    fn test_camera_zoom_control_clamping_down() {
        let mut app = App::new();

        app.add_event::<MouseWheel>();
        app.add_system(camera_zoom_control);

        // Setup test entities
        let camera_id = app
            .world
            .spawn(Camera2dBundle::default())
            .insert(MainCamera)
            .id();

        // custom scale to trigger clamping
        app.world
            .get_mut::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale = 0.13;

        app.world
            .resource_mut::<Events<MouseWheel>>()
            .send(MouseWheel {
                x: 0.0,
                y: 5.0, // no idea. but I'm only checkin >0 and <0 anyways
                unit: MouseScrollUnit::Line,
            });

        // Run systems
        app.update();

        let camera_scale = app
            .world
            .get::<OrthographicProjection>(camera_id)
            .unwrap()
            .scale;
        assert_eq!(camera_scale, CAMERA_MIN_ZOOM);
    }
}
