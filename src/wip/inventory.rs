use bevy::prelude::*;

#[derive(Debug, Reflect, Clone, PartialEq, FromReflect)]
pub enum ItemQuantity {
    One,
    #[allow(dead_code)]
    Many(usize),
}

#[derive(Component, Clone)]
pub struct PlayerInventoryTag;

#[derive(Debug, Component, Reflect, Clone, PartialEq, FromReflect)]
pub struct Item {
    #[allow(dead_code)]
    pub id: String,
    #[allow(dead_code)]
    pub item_quantity: ItemQuantity,
    pub icon: Handle<Image>,
}

#[derive(Debug, Reflect, Copy, Clone, PartialEq, FromReflect)]
pub enum InventorySlot {
    Empty,
    Entity(Entity),
}

impl Default for InventorySlot {
    fn default() -> Self {
        Self::Empty
    }
}

#[derive(Component)]
pub struct InventoryTag;

// TODO is PartialEq here enough ?
#[derive(Component, Reflect, PartialEq, Clone, Debug)]
#[derive(Default)]
pub struct Inventory {
    pub content: Vec<Vec<InventorySlot>>,
}



impl Inventory {
    pub fn insert_at(mut self, row: usize, col: usize, item: InventorySlot) -> Result<(), String> {
        let Some(row) = self.content.get_mut(row) else {
            return Err("fuck".into());
        };
        if col >= row.len() {
            return Err("Out of bounds".into());
        }
        let _ = std::mem::replace(&mut row[col], item);
        Ok(())
    }

    pub fn remove_at(mut self, row: usize, col: usize) -> Result<(), String> {
        let Some(row) = self.content.get_mut(row) else {
            return Err("fuck".into());
        };
        if col >= row.len() {
            return Err("Out of bounds".into());
        }
        row[col] = InventorySlot::Empty;
        Ok(())
    }
}

#[derive(Debug, Clone, Resource)]
pub struct WipInventoryIsOpen {
    pub is_open: bool,
}

pub fn toggle_inventory_ui(keyboard: Res<Input<KeyCode>>, mut is_open: ResMut<WipInventoryIsOpen>) {
    if keyboard.just_pressed(KeyCode::Tab) {
        info!("tab pressed, toggling player inventory");
        is_open.is_open = !is_open.is_open;
        info!("is_open => {:?}", *is_open);
    }
}
