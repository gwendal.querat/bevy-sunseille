use bevy::prelude::*;
use bevy::render::render_resource::Extent3d;
use bevy::window::PrimaryWindow;

#[derive(Debug, Component, Clone, PartialEq)]
pub struct ImageBeingDragged {
    pub dragged_entity: Entity,
    pub source_widget: Entity,
}

#[derive(Debug)]
pub struct DraggedAndDropped {
    #[allow(dead_code)] // not ded, used in logs
    dropped_position_on_window: Vec2,
}

fn get_image_size(image: &UiImage, images: &Res<Assets<Image>>) -> Extent3d {
    let image = images.get(&image.texture).unwrap();
    image.texture_descriptor.size
}

pub fn on_item_dragndrop(
    mut _commands: Commands,
    mut evt_reader: EventReader<DraggedAndDropped>,
    query_ui: Query<(Entity, &Transform, &Style), With<Node>>,
) {
    for event in evt_reader.iter() {
        info!("item dragndropped => {:?}", event);
        for (_entity, transform, style) in query_ui.iter() {
            info!("transform {:?}", transform);
            info!("style.position {:?}", style.position);
        }
    }
}

pub fn dragndrop(
    mut commands: Commands,
    mut dragged_query: Query<(Entity, &mut Style, &UiImage), With<ImageBeingDragged>>,
    mouse: Res<Input<MouseButton>>,
    window_query: Query<&Window, With<PrimaryWindow>>,
    images: Res<Assets<Image>>,
    mut evt_dropped: EventWriter<DraggedAndDropped>,
) {
    // return early to avoid spamming logs
    if dragged_query.iter().len() == 0 {
        return;
    }

    let Ok(window) = window_query.get_single() else {
        error!("Could not get primary window");
        return;
    };

    let Some(position) = window.cursor_position() else {
        info!("Could not get cursor position (outside of window ?)");
        return;
    };

    for (dragged_sprite_entity, mut style, ui_image) in dragged_query.iter_mut() {
        let dragged_sprite_entity = commands.entity(dragged_sprite_entity);
        let image_size = get_image_size(ui_image, &images);

        // todo this is duplicated code
        let width = (image_size.width as f32).min(48.);
        let height = (image_size.height as f32).min(48.);

        style.position = UiRect::new(
            Val::Px(position.x - (width / 2.)),
            Val::DEFAULT,
            Val::DEFAULT,
            Val::Px(position.y - (height / 2.)),
        );
        if mouse.just_released(MouseButton::Left) {
            info!("removing dragged sprite {:?}", dragged_sprite_entity.id());
            evt_dropped.send(DraggedAndDropped {
                dropped_position_on_window: Vec2::new(position.x, position.y),
            });
            dragged_sprite_entity.despawn_recursive();
            continue;
        }
    }
}
