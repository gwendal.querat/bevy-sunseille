use bevy::prelude::*;

#[derive(Debug)]
pub struct EvtChangeAnimationRequest {
    pub entity: Entity,
    pub animation: String,
}

pub struct EvtAnimationEnded {
    pub entity_id: Entity,
}

#[derive(Debug)]
pub struct EvtMeleeAttackRequested(pub Entity);
#[derive(Debug)]
pub struct EvtMeleeAttackStarted(pub Entity);
#[derive(Debug)]
pub struct AttackCooldownEnded(pub Entity);

#[derive(Debug)]
pub struct EvtEnteredSensorRange {
    pub entity_with_sensor_child: Entity,
    pub entity_entering_sensor_range: Entity,
}
#[derive(Debug)]
pub struct EvtExitedSensorRange {
    pub entity_with_sensor_child: Entity,
    pub entity_exiting_sensor_range: Entity,
}

#[derive(Debug)]
pub struct EvtPlayerInteraction {
    pub player_entity: Entity,
    pub interactable_entity: Entity,
}
