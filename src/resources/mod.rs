use crate::Animation;
use bevy::prelude::*;
use bevy::utils::HashMap;

#[derive(Default, Resource)]
pub struct AnimationsDict {
    pub entries: HashMap<String, Animation>,
}
