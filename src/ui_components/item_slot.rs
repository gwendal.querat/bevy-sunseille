use crate::wip::dragndrop::ImageBeingDragged;
use bevy::ecs::query::QuerySingleError;
use bevy::prelude::*;
use bevy::render::render_resource::Extent3d;
use kayak_ui::prelude::EventType::*;
use kayak_ui::prelude::*;
use kayak_ui::widgets::{ElementBundle, KImage, KImageBundle};

use crate::wip::inventory::{InventorySlot, Item};

#[derive(Bundle)]
pub struct ItemSlotUiBundle {
    pub props: ItemSlotUiProps,
    pub styles: KStyle,
    pub children: KChildren,
    // This allows us to hook into on click events!
    pub on_event: OnEvent,
    // Widget name is required by Kayak UI!
    pub widget_name: WidgetName,
}

impl Default for ItemSlotUiBundle {
    fn default() -> Self {
        Self {
            props: ItemSlotUiProps::default(),
            styles: KStyle::default(),
            children: KChildren::default(),
            on_event: OnEvent::default(),
            widget_name: ItemSlotUiProps::default().get_name(),
        }
    }
}

#[derive(Component, Debug, Clone, PartialEq)]
pub struct ItemSlotUiProps {
    pub item_slot: InventorySlot,
    pub row: usize,
    pub col: usize,
}

impl Default for ItemSlotUiProps {
    fn default() -> Self {
        Self {
            item_slot: InventorySlot::Empty,
            // This should force an error log if you don't override these values
            row: usize::MAX,
            col: usize::MAX,
        }
    }
}

impl Widget for ItemSlotUiProps {}

pub fn setup_item_slot_ui(widget_context: &mut KayakRootContext) {
    // We need to register the prop and state types.
    // State is empty so you can use the `EmptyState`
    // component!
    widget_context.add_widget_data::<ItemSlotUiProps, EmptyState>();

    // Next we need to add the systems
    widget_context.add_widget_system(
        // We are registering these systems with a specific
        // WidgetName.
        ItemSlotUiProps::default().get_name(),
        widget_update::<ItemSlotUiProps, EmptyState>,
        // Add our render system!
        render_item_slot_ui,
    );
}

fn get_slot_style() -> KStyle {
    KStyle {
        padding: StyleProp::Value(Edge::all(Units::Percentage(5.))),
        background_color: StyleProp::Value(Color::TEAL),
        render_command: StyleProp::Value(RenderCommand::Quad),
        ..default()
    }
}

fn get_slot_inside_style() -> KStyle {
    KStyle {
        padding: StyleProp::Value(Edge::all(Units::Stretch(1.))),
        background_color: StyleProp::Value(Color::LIME_GREEN),
        render_command: StyleProp::Value(RenderCommand::Quad),
        ..default()
    }
}

fn get_item_icon_style() -> KStyle {
    KStyle {
        width: StyleProp::Value(Units::Pixels(48.0)),
        height: StyleProp::Value(Units::Pixels(48.0)),
        ..default()
    }
}

pub fn on_mouse_up(
    In((event_dispatcher_context, _widget_state, event, _entity)): In<(
        EventDispatcherContext,
        WidgetState,
        KEvent,
        Entity,
    )>,
    query_item: Query<(Entity, &Item)>,
    query_dragged: Query<&ImageBeingDragged>,
    mut query_item_slot_props: Query<(Entity, &mut ItemSlotUiProps)>,
    target_widget_entity: Entity,
) -> (EventDispatcherContext, KEvent) {
    info!("coin");

    let dragndrop_entity = match query_dragged.get_single() {
        Ok(x) => x,
        Err(error) => {
            match error {
                QuerySingleError::NoEntities(_) => {
                    trace!("mouse up with no entity being dragged");
                }
                QuerySingleError::MultipleEntities(_) => {
                    error!("mouse up with multiple entities having ImageBeingDragged");
                }
            }
            return (event_dispatcher_context, event);
        }
    };
    info!("mouse up with dragged image over item slot");

    let Ok((item_entity, item)) = query_item.get(dragndrop_entity.dragged_entity) else {
        error!("coincoin");
        return (event_dispatcher_context, event);
    };

    info!("item: {:?}", item);

    let (_, mut target_item_slot) = query_item_slot_props.get_mut(target_widget_entity).unwrap();
    target_item_slot.item_slot = InventorySlot::Entity(item_entity);

    let (_, mut source_item_slot) = query_item_slot_props
        .get_mut(dragndrop_entity.source_widget)
        .unwrap(); // Bad. Not handling source widget despawning between start and end of click

    // Avoid removing the item if it is moved to the slot where it currently is
    if dragndrop_entity.source_widget == target_widget_entity {
        return (event_dispatcher_context, event);
    }

    source_item_slot.item_slot = InventorySlot::Empty;
    // TODO Update ingame inventory

    (event_dispatcher_context, event)
}

pub fn render_item_slot_ui(
    In((widget_context, entity)): In<(KayakWidgetContext, Entity)>,
    mut commands: Commands,
    item_slot_props: Query<&ItemSlotUiProps, &KStyle>,
    query_item: Query<(Entity, &Item)>,
) -> bool {
    let parent_id = Some(entity);

    let item_slot_props = item_slot_props
        .get(entity)
        .expect("props not found for ui element");

    let handle_click =
        move |In((event_dispatcher_context, _widget_state, mut event, _entity)): In<(
            EventDispatcherContext,
            WidgetState,
            KEvent,
            Entity,
        )>,
              item_slot_props: Query<(Entity, &mut ItemSlotUiProps)>,
              mut commands: Commands,
              images: Res<Assets<Image>>,
              query_item: Query<(Entity, &Item)>,
              query_dragged: Query<&ImageBeingDragged>|
              -> (EventDispatcherContext, KEvent) {
            let cursor_event = match event.event_type {
                MouseDown(cursor_event) => cursor_event,
                MouseUp(_cursor_event) => {
                    return on_mouse_up(
                        In((event_dispatcher_context, _widget_state, event, _entity)),
                        query_item,
                        query_dragged,
                        item_slot_props,
                        entity,
                    )
                }
                _ => return (event_dispatcher_context, event),
            };

            event.stop_propagation();
            event.prevent_default();

            if !cursor_event.just_pressed {
                return (event_dispatcher_context, event);
            }
            info!("click ! {:?}", cursor_event);
            let Ok((widget_entity, props)) = item_slot_props.get(entity) else {
                error!("item_slot_props not found");
                return (event_dispatcher_context, event);
            };
            let InventorySlot::Entity(entity) = &props.item_slot else {
                info!("Nothing in item_slot.");
                return (event_dispatcher_context, event);
            };
            let (ent, item) = query_item.get(*entity).unwrap();

            info!("spawning sprite to drag and drop");

            let image_size = images
                .get(&item.icon)
                .map(|image| image.texture_descriptor.size)
                .or_else(|| {
                    warn!("Could not get item's icon. using default icon size");
                    Some(Extent3d {
                        width: 32,
                        height: 32,
                        ..default()
                    })
                })
                .unwrap();

            let width = (image_size.width as f32).min(48.);
            let height = (image_size.height as f32).min(48.);

            let bevy_ui_size = bevy::prelude::Size::new(Val::Px(width), Val::Px(height));

            commands
                .spawn(ImageBundle {
                    image: UiImage::new(item.icon.clone()),
                    style: Style {
                        // force image size
                        max_size: bevy_ui_size,
                        min_size: bevy_ui_size,
                        // absolute positioning => Images is where I want it to be
                        position_type: PositionType::Absolute,
                        // center the item on the click's position
                        position: UiRect::new(
                            Val::Px(cursor_event.position.0 - (width / 2.)),
                            Val::DEFAULT,
                            Val::Px(cursor_event.position.1 - (height / 2.)),
                            Val::DEFAULT,
                        ),
                        ..default()
                    },
                    ..default()
                })
                .insert(ImageBeingDragged {
                    dragged_entity: ent,
                    source_widget: widget_entity,
                });

            (event_dispatcher_context, event)
        };
    let handle_click = OnEvent::new(handle_click);

    let _entity_id = rsx! {
        <ElementBundle styles={get_slot_style()} on_event={handle_click}>
            <ElementBundle styles={get_slot_inside_style()}>
                {
                    match &item_slot_props.item_slot {
                        InventorySlot::Entity(entity) => {
                            // TODO handle entities without <Item>
                            let (_, item) = query_item.get(*entity).unwrap();

                            constructor! {
                                <KImageBundle
                                    image={KImage(item.icon.clone())}
                                    styles={get_item_icon_style()}
                                />
                            };
                        },
                        InventorySlot::Empty => {
                            constructor! {
                                <ElementBundle styles={get_item_icon_style()}/>
                            };
                        }
                    }
                }
            </ElementBundle>
        </ElementBundle>
    };

    true
}
