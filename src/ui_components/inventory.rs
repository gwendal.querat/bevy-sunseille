use bevy::prelude::*;
use kayak_ui::prelude::LayoutType::Row;
use kayak_ui::prelude::*;
use kayak_ui::widgets::ElementBundle;

use crate::ui_components::item_slot::{ItemSlotUiBundle, ItemSlotUiProps};
use crate::wip::inventory::Inventory;
use crate::WipInventoryIsOpen;

#[derive(Bundle)]
pub struct InventoryUiBundle {
    pub props: InventoryUiProps,
    pub styles: KStyle,
    pub children: KChildren,
    // This allows us to hook into on click events!
    pub on_event: OnEvent,
    // Widget name is required by Kayak UI!
    pub widget_name: WidgetName,
}

impl Default for InventoryUiBundle {
    fn default() -> Self {
        Self {
            props: InventoryUiProps::default(),
            styles: KStyle::default(),
            children: KChildren::default(),
            on_event: OnEvent::default(),
            widget_name: InventoryUiProps::default().get_name(),
        }
    }
}

#[derive(Clone, Component, PartialEq)]
#[derive(Default)]
pub struct InventoryUiProps {
    pub inventory: Option<Inventory>,
}



impl Widget for InventoryUiProps {}

pub fn setup_inventory_ui(widget_context: &mut KayakRootContext) {
    // We need to register the prop and state types.
    // State is empty so you can use the `EmptyState`
    // component!
    widget_context.add_widget_data::<InventoryUiProps, EmptyState>();

    // Next we need to add the systems
    widget_context.add_widget_system(
        // We are registering these systems with a specific
        // WidgetName.
        InventoryUiProps::default().get_name(),
        widget_update_with_resource::<InventoryUiProps, EmptyState>,
        // Add our render system!
        render_inventory_ui,
    );
}

fn get_row_styles() -> KStyle {
    KStyle {
        layout_type: StyleProp::Value(Row),
        ..default()
    }
}

fn get_inventory_styles() -> KStyle {
    KStyle {
        position_type: StyleProp::Value(KPositionType::SelfDirected),
        left: StyleProp::Value(Units::Percentage(30.)),
        right: StyleProp::Value(Units::Percentage(30.)),
        top: StyleProp::Value(Units::Percentage(30.)),
        bottom: StyleProp::Value(Units::Percentage(30.)),
        render_command: StyleProp::Value(RenderCommand::Quad),
        background_color: StyleProp::Value(Color::from([0., 0., 0.])),
        ..default()
    }
}

// Our own version of widget_update that handles resource change events.
pub fn widget_update_with_resource<
    Props: PartialEq + Component + Clone,
    State: PartialEq + Component + Clone,
>(
    In((widget_context, entity, previous_entity)): In<(KayakWidgetContext, Entity, Entity)>,
    my_resource: Res<WipInventoryIsOpen>,
    widget_param: WidgetParam<Props, State>,
) -> bool {
    widget_param.has_changed(&widget_context, entity, previous_entity) || my_resource.is_changed()
}

pub fn render_inventory_ui(
    In((widget_context, entity)): In<(KayakWidgetContext, Entity)>,
    mut commands: Commands,
    inventory_props_query: Query<&InventoryUiProps>,
    wip_inventory_is_open: Res<WipInventoryIsOpen>,
) -> bool {
    let parent_id = Some(entity);

    if !wip_inventory_is_open.is_open {
        return true;
    }

    // TODO now: pass inventory via props
    if let Ok(inventory) = inventory_props_query.get(entity) {
        let Some(ref inventory) = inventory.inventory else {
            error!("fuck");
            return true;
        };

        let _entity_id = rsx! {
            <ElementBundle styles={get_inventory_styles()}>
                {
                    inventory.content.iter().enumerate().for_each(|(row_number, row)| {
                        constructor! {
                            <ElementBundle styles={get_row_styles()}>
                            {
                                row.iter().enumerate().for_each(|(col_number, item_slot)|{
                                    constructor! {
                                        <ItemSlotUiBundle
                                            props={ItemSlotUiProps{
                                              item_slot: *item_slot,
                                              row: row_number,
                                              col: col_number
                                            }}
                                        />
                                    }
                                })
                            }
                            </ElementBundle>
                        }
                    })
                }
            </ElementBundle>
        };
    } else {
        error!("No entity with PlayerComponent and Inventory found");
    }
    true
}
