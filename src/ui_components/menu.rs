use crate::assets::images::ImageAssets;
use bevy::prelude::*;
use kayak_ui::prelude::*;
use kayak_ui::widgets::{ElementBundle, NinePatch, NinePatchBundle, TextProps, TextWidgetBundle};

#[derive(Default, Component, Debug, Clone, PartialEq)]
pub struct MenuProps;

impl Widget for MenuProps {}

#[derive(Bundle, Debug)]
pub struct MenuBundle {
    pub props: MenuProps,
    pub style: KStyle,
    pub children: KChildren,
    pub on_event: OnEvent,
    pub widget_name: WidgetName,
}

impl Default for MenuBundle {
    fn default() -> Self {
        Self {
            props: MenuProps::default(),
            style: KStyle::default(),
            children: KChildren::default(),
            on_event: OnEvent::default(),
            widget_name: MenuProps::default().get_name(),
        }
    }
}

fn get_menu_styles() -> KStyle {
    KStyle {
        border_radius: StyleProp::Value(Corner::all(15.0)),
        background_color: StyleProp::Value(Color::WHITE),
        bottom: StyleProp::Value(Units::Stretch(1.0)),
        height: StyleProp::Value(Units::Pixels(500.0)),
        layout_type: StyleProp::Value(LayoutType::Column),
        left: StyleProp::Value(Units::Stretch(1.0)),
        padding: StyleProp::Value(Edge::all(Units::Stretch(1.))),
        right: StyleProp::Value(Units::Stretch(1.0)),
        row_between: StyleProp::Value(Units::Pixels(20.0)),
        top: StyleProp::Value(Units::Stretch(1.0)),
        width: StyleProp::Value(Units::Pixels(360.0)),
        ..Default::default()
    }
}

fn render_menu_ui_bundle(
    In((widget_context, entity)): In<(KayakWidgetContext, Entity)>,
    mut commands: Commands,
    _npc_dialog_ui_props: Query<&MenuProps>,
    images: Res<ImageAssets>,
) -> bool {
    let parent_id = Some(entity);
    rsx! {
        <ElementBundle>
            <NinePatchBundle
                styles={get_menu_styles()}
                nine_patch={NinePatch {
                    handle: images.panel.clone(),
                    border:{Edge::all(10.0)}
                }}
            >
                <TextWidgetBundle
                    text={TextProps {
                        content: "pouêt pouêt pouêt".into(),
                        size: 20.0,
                        ..Default::default()
                    }}
                />
            </NinePatchBundle>
        </ElementBundle>
    };

    true
}

pub fn setup_menu_ui(widget_context: &mut KayakRootContext) {
    // We need to register the prop and state types.
    // State is empty so you can use the `EmptyState`
    // component!
    widget_context.add_widget_data::<MenuProps, EmptyState>();

    // Next we need to add the systems
    widget_context.add_widget_system(
        // We are registering these systems with a specific
        // WidgetName.
        MenuProps::default().get_name(),
        // widget_update auto diffs props and state.
        // Optionally if you have context you can use:
        // widget_update_with_context otherwise you
        // will need to create your own widget update
        // system!
        widget_update::<MenuProps, EmptyState>,
        // Add our render system!
        render_menu_ui_bundle,
    );
}
