use bevy::prelude::*;

use kayak_ui::prelude::*;
use kayak_ui::widgets::{ElementBundle, TextProps, TextWidgetBundle};

use crate::ui_components::fps_counter::FpsCounterUiBundle;
// use crate::ui_components::inventory::InventoryUiBundle;
use crate::ui_components::inventory::{InventoryUiBundle, InventoryUiProps};

use crate::plugins::player::PlayerComponent;
use crate::ui_components::menu::MenuBundle;
use crate::ui_components::npc_dialog::NpcDialogUiBundle;
use crate::wip::inventory::Inventory;
use crate::GameState;

#[derive(Debug, Component, Clone, PartialEq, Default)]
pub struct RootUiProps {}

impl RootUiProps {
    pub fn _new() -> Self {
        Self {}
    }
}

// In the future this will tell Kayak that these
// Props belongs to a widget. For now it's use to
// get the `WidgetName` component.
impl Widget for RootUiProps {}

// Now we need a widget bundle this can represent a collection of components our widget might have
// Note: You can include custom data here. Just don't expect it to get diffed during update!
#[derive(Bundle)]
pub struct RootUiBundle {
    pub props: RootUiProps,
    pub styles: KStyle,
    pub children: KChildren,
    // This allows us to hook into on click events!
    pub on_event: OnEvent,
    // Widget name is required by Kayak UI!
    pub widget_name: WidgetName,
}

impl Default for RootUiBundle {
    fn default() -> Self {
        Self {
            props: RootUiProps::default(),
            styles: KStyle::default(),
            children: KChildren::default(),
            on_event: OnEvent::default(),
            widget_name: RootUiProps::default().get_name(),
        }
    }
}

fn should_root_ui_update(
    In((_widget_context, _entity, _prev)): In<(KayakWidgetContext, Entity, Entity)>,
    current_state: Res<State<GameState>>,
) -> bool {
    if current_state.is_changed() {
        info!(
            "kayak_ui will update - state changed (to {:?})",
            current_state.0
        );
        return true;
    }
    false
}

pub fn setup_root_ui_bundle(widget_context: &mut KayakRootContext) {
    // We need to register the prop and state types.
    // State is empty so you can use the `EmptyState`
    // component!
    widget_context.add_widget_data::<RootUiProps, EmptyState>();

    // Next we need to add the systems
    widget_context.add_widget_system(
        // We are registering these systems with a specific
        // WidgetName.
        RootUiProps::default().get_name(),
        // widget_update auto diffs props and state.
        // Optionally if you have context you can use:
        // widget_update_with_context otherwise you
        // will need to create your own widget update
        // system!
        should_root_ui_update,
        // Add our render system!
        render_root_ui,
    );
}

fn get_root_ui_styles() -> KStyle {
    KStyle {
        position_type: StyleProp::Value(KPositionType::SelfDirected),
        ..default()
    }
}

fn get_style() -> KStyle {
    KStyle {
        layout_type: StyleProp::Value(LayoutType::Column),
        position_type: StyleProp::Value(KPositionType::SelfDirected),
        padding: StyleProp::Value(Edge::all(Units::Percentage(1.))),
        ..default()
    }
}

#[allow(non_snake_case)]
pub fn render_root_ui(
    // This is a bevy feature which allows custom
    // parameters to be passed into a system.
    // In this case Kayak UI gives the system a
    // `KayakWidgetContext` and an `Entity`.
    In((widget_context, entity)): In<(KayakWidgetContext, Entity)>,
    mut commands: Commands,
    _main_menu_props: Query<&RootUiProps>,
    game_current_state: Res<State<GameState>>,
    query_inventory: Query<&Inventory, With<PlayerComponent>>,
) -> bool {
    // Not used, kept as learning example to get props in case you need their data
    let _main_menu_props = _main_menu_props.get(entity).unwrap();
    let parent_id = Some(entity);

    let mut passed_inventory_copy: Option<Inventory> = None;
    if let Ok(inventory) = query_inventory.get_single() {
        passed_inventory_copy = Some(inventory.clone());
    };
    info!("rendering ui. state: {:?}", game_current_state.0);
    rsx! {
        <ElementBundle styles={get_style()}>
            <FpsCounterUiBundle/>
            <ElementBundle styles={get_root_ui_styles()}>
            {
                match game_current_state.0 {
                    GameState::Menu => {
                        constructor! {
                            <MenuBundle/>
                        }
                    },
                    GameState::InGame => {
                        constructor! {
                            <ElementBundle>
                                <TextWidgetBundle
                                    text={TextProps {
                                        content: "InGame. Esc => menu, E => talk to slime (need to be close)".into(),
                                        size: 20.0,
                                        ..Default::default()
                                    }}
                                />
                                <InventoryUiBundle
                                    props={
                                        InventoryUiProps {
                                            inventory: passed_inventory_copy,
                                            ..Default::default()
                                        }
                                    }
                                />
                            </ElementBundle>
                        }
                    },
                    GameState::TalkingToNpc => {
                        constructor! {
                            <NpcDialogUiBundle/>
                        }
                    }
                }
            }
            </ElementBundle>
        </ElementBundle>
    };

    true
}
