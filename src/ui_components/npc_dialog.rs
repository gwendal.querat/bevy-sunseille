use crate::assets::images::ImageAssets;
use bevy::prelude::*;
use kayak_ui::prelude::*;
use kayak_ui::widgets::{ElementBundle, NinePatch, NinePatchBundle, TextProps, TextWidgetBundle};

#[derive(Debug, Clone, Component, PartialEq, Default)]
pub struct NpcDialogUiProps {}

#[derive(Debug, Bundle)]
pub struct NpcDialogUiBundle {
    pub props: NpcDialogUiProps,
    pub styles: KStyle,
    pub children: KChildren,
    // This allows us to hook into on click events!
    pub on_event: OnEvent,
    // Widget name is required by Kayak UI!
    pub widget_name: WidgetName,
}

impl Default for NpcDialogUiBundle {
    fn default() -> Self {
        Self {
            props: NpcDialogUiProps::default(),
            styles: KStyle::default(),
            children: KChildren::default(),
            on_event: OnEvent::default(),
            widget_name: NpcDialogUiProps::default().get_name(),
        }
    }
}

// In the future this will tell Kayak that these
// Props belongs to a widget. For now it's use to
// get the `WidgetName` component.
impl Widget for NpcDialogUiProps {}

fn get_npc_textbox_style() -> KStyle {
    KStyle {
        border_radius: StyleProp::Value(Corner::all(15.0)),
        layout_type: StyleProp::Value(LayoutType::Column),
        padding: StyleProp::Value(Edge::all(Units::Stretch(1.))),
        top: StyleProp::Value(Units::Stretch(1.0)),
        left: StyleProp::Value(Units::Stretch(1.0)),
        right: StyleProp::Value(Units::Stretch(1.0)),
        bottom: StyleProp::Value(Units::Percentage(5.0)),
        height: StyleProp::Value(Units::Percentage(20.0)),
        width: StyleProp::Value(Units::Percentage(90.0)),
        ..Default::default()
    }
}

pub fn get_text_style() -> KStyle {
    KStyle {
        background_color: StyleProp::Value(Color::RED),
        color: StyleProp::Value(Color::BLACK),
        ..Default::default()
    }
}

fn get_style() -> KStyle {
    KStyle {
        position_type: StyleProp::Value(KPositionType::SelfDirected),
        height: StyleProp::Value(Units::Percentage(100.)),
        width: StyleProp::Value(Units::Percentage(100.)),

        ..Default::default()
    }
}

pub fn render_npc_dialog_ui_bundle(
    In((widget_context, entity)): In<(KayakWidgetContext, Entity)>,
    mut commands: Commands,
    _npc_dialog_ui_props: Query<&NpcDialogUiProps>,
    images: Res<ImageAssets>,
) -> bool {
    let parent_id = Some(entity);

    info!("rendering npc dialog");

    // The rsx! macro expects a parent_id, a widget_context from the user.
    // It also expects `Commands` from bevy.
    // This can be a little weird at first.
    // See the rsx! docs for more info!
    let _entity_id = rsx! {
        <ElementBundle styles={get_style()}>
            <TextWidgetBundle
                text={TextProps {
                    content: "Talking to NPC".into(),
                    size: 20.0,
                    ..Default::default()
                }}
            />
            <NinePatchBundle
                styles={get_npc_textbox_style()}
                nine_patch={NinePatch {
                    handle: images.panel_light_4.clone(),
                    border:{Edge::all(10.0)}
                }}
            >
                <TextWidgetBundle
                    styles={get_text_style()}
                    text={TextProps {
                        content: "Talking to NPC".into(),
                        size: 20.0,
                        ..Default::default()
                    }}
                />
            </NinePatchBundle>
        </ElementBundle>
    };

    true
}

pub fn setup_npc_dialog_ui_bundle(widget_context: &mut KayakRootContext) {
    // We need to register the prop and state types.
    // State is empty so you can use the `EmptyState`
    // component!
    widget_context.add_widget_data::<NpcDialogUiProps, EmptyState>();

    // Next we need to add the systems
    widget_context.add_widget_system(
        // We are registering these systems with a specific
        // WidgetName.
        NpcDialogUiProps::default().get_name(),
        // widget_update auto diffs props and state.
        // Optionally if you have context you can use:
        // widget_update_with_context otherwise you
        // will need to create your own widget update
        // system!
        widget_update::<NpcDialogUiProps, EmptyState>,
        // Add our render system!
        render_npc_dialog_ui_bundle,
    );
}
