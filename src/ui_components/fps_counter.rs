use bevy::diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin};
use bevy::prelude::*;

use kayak_ui::prelude::Units::Stretch;
use kayak_ui::prelude::*;
use kayak_ui::widgets::{TextProps, TextWidgetBundle};

#[derive(Bundle)]
pub struct FpsCounterUiBundle {
    pub props: EmptyState,
    pub styles: KStyle,
    pub children: KChildren,
    // This allows us to hook into on click events!
    pub on_event: OnEvent,
    // Widget name is required by Kayak UI!
    pub widget_name: WidgetName,
}

impl Default for FpsCounterUiBundle {
    fn default() -> Self {
        Self {
            props: EmptyState::default(),
            styles: KStyle::default(),
            children: KChildren::default(),
            on_event: OnEvent::default(),
            widget_name: FpsCounterUiProps::default().get_name(),
        }
    }
}

#[derive(Default, Component, Clone, PartialEq)]
pub struct FpsCounterUiProps {}

impl Widget for FpsCounterUiProps {}

pub fn setup_fps_counter_ui(widget_context: &mut KayakRootContext) {
    // We need to register the prop and state types.
    // State is empty so you can use the `EmptyState`
    // component!
    widget_context.add_widget_data::<FpsCounterUiProps, EmptyState>();

    // Next we need to add the systems
    widget_context.add_widget_system(
        // We are registering these systems with a specific
        // WidgetName.
        FpsCounterUiProps::default().get_name(),
        // custom "Should update" function that forces a render every frame
        |In((_widget_context, _entity, _prev)): In<(KayakWidgetContext, Entity, Entity)>| true,
        // Add our render system!
        render_fps_counter_ui,
    );
}

fn get_fps(diagnostics: Res<Diagnostics>) -> String {
    if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
        if let Some(average) = fps.average() {
            return format!("{average:.1}");
        }
        // Not checking the return of fps.average since it
        // only returns None when there is no FPS history
        // (at game start for example).
        warn!("Could not get average FPS");
    } else {
        error!("diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) failed");
    }
    String::from("Could not get FPS")
}

fn get_style() -> KStyle {
    KStyle {
        left: StyleProp::Value(Stretch(1.)),
        position_type: StyleProp::Value(KPositionType::SelfDirected),
        ..default()
    }
}

pub fn render_fps_counter_ui(
    In((widget_context, entity)): In<(KayakWidgetContext, Entity)>,
    mut commands: Commands,
    diagnostics: Res<Diagnostics>,
) -> bool {
    let fps = get_fps(diagnostics);

    let parent_id = Some(entity);
    let _entity_id = rsx! {
        <TextWidgetBundle
            styles={get_style()}
            text={TextProps {
                content: fps,
                size: 20.0,
                ..Default::default()
            }}
        />
    };

    true
}
