use bevy::prelude::*;
use bevy_asset_loader::prelude::*;

#[derive(AssetCollection, Resource)]
pub struct ImageAssets {
    #[asset(path = "gb_studio_ui_asset_pack/frames/MED_BG/med_frame_02.png")]
    pub panel: Handle<Image>,
    #[asset(path = "gb_studio_ui_asset_pack/frames/LIGHT BG/light frame 04.png")]
    pub panel_light_4: Handle<Image>,
}

pub struct ImageAssetsPlugin;

impl Plugin for ImageAssetsPlugin {
    fn build(&self, app: &mut App) {
        app.init_collection::<ImageAssets>();
    }
}
