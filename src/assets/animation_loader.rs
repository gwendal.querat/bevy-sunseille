use bevy::asset::{AssetLoader, BoxedFuture, Error, LoadContext, LoadedAsset};

use crate::components::animations::Animation;

#[derive(Default)]
pub struct AnimationLoader;

// Taken from benimator's example
impl AssetLoader for AnimationLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, anyhow::Result<(), Error>> {
        Box::pin(async move {
            // `Animation` implements `serde::Deserialize`,
            // so you may use any serde-compatible deserialization library
            let animation: Animation = Animation(serde_yaml::from_slice(bytes)?);
            load_context.set_default_asset(LoadedAsset::new(animation));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["animation.yml"]
    }
}
