extern crate core;

use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::log::LogPlugin;
use bevy::prelude::*;
use bevy::window::WindowResolution;
use bevy_ecs_ldtk::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_rapier2d::prelude::*;
use kayak_ui::prelude::{widgets::*, *};

use components::animations::Animation;
use plugins::player::PlayerPlugin;
use plugins::slime::SlimePlugin;
use systems::camera::camera_translation_control;
use systems::interactions::{
    animate_slime_on_interaction, detect_player_interaction,
    highlight_interactable_on_player_proximity, stop_highlighting_interactable_on_player_proximity,
};

use crate::assets::animation_loader::*;
use crate::assets::images::ImageAssetsPlugin;
use crate::events::{
    EvtAnimationEnded, EvtChangeAnimationRequest, EvtEnteredSensorRange, EvtExitedSensorRange,
    EvtPlayerInteraction,
};
use crate::plugins::player::{
    player_animation_reset_to_idle, player_attack_scheduler, player_interaction,
    tick_and_remove_attack_cooldown,
};
use crate::plugins::slime::{make_slime_idle_on_animation_end, test_slime_animation_with_keyboard};
use crate::resources::AnimationsDict;
use crate::systems::camera::camera_zoom_control;
use crate::systems::character_animation::character_animation;
use crate::systems::interactions::{talk_to_npc, TmpPlugin};
use crate::systems::physics::{pause_rapier, resume_rapier};
use crate::systems::sensor::{sensor_system, sensor_system_debug};
use crate::ui_components::root_ui::*;
use crate::wip::dragndrop::{dragndrop, on_item_dragndrop, DraggedAndDropped};
use crate::wip::inventory::{toggle_inventory_ui, WipInventoryIsOpen};

mod assets;
mod bundles;
mod components;
mod constants;
mod events;
mod plugins;
mod resources;
mod systems;
mod ui;
mod ui_components;
mod wip;

#[test]
#[allow(clippy::assertions_on_constants)]
fn test_gitlab_ci() {
    assert!(true);
}

/// Our Application State
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, States)]
pub enum GameState {
    Menu,
    InGame,
    TalkingToNpc,
}

impl Default for GameState {
    fn default() -> Self {
        Self::InGame
    }
}

fn startup_load_ldtk(asset_server: Res<AssetServer>, mut commands: Commands) {
    commands.spawn(LdtkWorldBundle {
        ldtk_handle: asset_server.load("world.ldtk"),
        ..default()
    });
}

fn spawn_tiles_collisions(
    query: Query<(Entity, &Transform, &IntGridCell), Added<IntGridCell>>,
    mut commands: Commands,
) {
    for (entity, transform, grid_cell) in query.iter() {
        if grid_cell.value == 1 {
            commands.entity(entity).insert((
                RigidBody::Fixed,
                Collider::cuboid(8., 8.),
                Ccd::disabled(),
                Dominance { groups: 127 },
                TransformBundle {
                    local: Transform {
                        translation: transform.translation,
                        ..default()
                    },
                    ..default()
                },
            ));
        }
    }
}

fn open_menu_on_esc_press(mut commands: Commands, keyboard: Res<Input<KeyCode>>) {
    if keyboard.just_pressed(KeyCode::Escape) {
        commands.insert_resource(NextState(Some(GameState::Menu)));
    }
}

fn exit_menu_on_esc_pressed(mut commands: Commands, keyboard: Res<Input<KeyCode>>) {
    if keyboard.just_pressed(KeyCode::Escape) {
        // TODO return to previous known state instead
        commands.insert_resource(NextState(Some(GameState::InGame)));
    }
}

fn main() {
    App::new()
        .add_state::<GameState>()
        // Bevy
        .add_plugins(
            DefaultPlugins
                .set(LogPlugin {
                    filter: "\
                        info,\
                        kayak_ui=debug,\
                        wgpu_core=warn,\
                        wgpu_hal=warn,\
                        egui=debug,\
                        bevy_egui=info,\
                        sunseille=info,\
                        sunseille::player=trace,\
                        sunseille::systems::interactions=debug,\
                        sunseille::ui_components=trace,\
                        sunseille::wip::inventory=trace,\
                        sunseille::wip::dragndrop=trace,\
                    "
                    .into(),
                    level: bevy::log::Level::DEBUG,
                })
                .set(ImagePlugin::default_nearest())
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        resolution: WindowResolution::new(1920., 1080.),
                        title: String::from("Map Example"),
                        ..Default::default()
                    }),
                    ..default()
                }),
        )
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(16.0))
        .add_plugin(RapierDebugRenderPlugin::default())
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugin(KayakContextPlugin)
        .add_plugin(KayakWidgets)
        .add_startup_system(ui::startup_kayak)
        // Third party libs
        .add_plugin(LdtkPlugin)
        .add_plugin(ImageAssetsPlugin)
        .add_startup_system(startup_load_ldtk)
        .insert_resource(WipInventoryIsOpen { is_open: false })
        .insert_resource(LevelSelection::Identifier("Level_0".to_string()))
        .insert_resource(LdtkSettings {
            level_spawn_behavior: LevelSpawnBehavior::UseWorldTranslation {
                load_level_neighbors: true,
            },
            set_clear_color: SetClearColor::FromLevelBackground,
            ..Default::default()
        })
        .insert_resource(AnimationsDict::default())
        .add_asset::<Animation>()
        .init_asset_loader::<AnimationLoader>()
        .add_event::<EvtEnteredSensorRange>()
        .add_event::<EvtExitedSensorRange>()
        .add_event::<EvtChangeAnimationRequest>()
        .add_event::<EvtAnimationEnded>()
        .add_event::<EvtPlayerInteraction>()
        .add_event::<DraggedAndDropped>()
        .add_plugin(WorldInspectorPlugin::default())
        // ENTER GameState::Ingame
        .add_systems((resume_rapier,).in_schedule(OnEnter(GameState::InGame)))
        // UPDATE GameState::InGame
        .add_system(dragndrop.in_set(OnUpdate(GameState::InGame)))
        .add_system(on_item_dragndrop.in_set(OnUpdate(GameState::InGame)))
        .add_system(open_menu_on_esc_press.in_set(OnUpdate(GameState::InGame)))
        .add_system(character_animation.in_set(OnUpdate(GameState::InGame)))
        .add_system(camera_translation_control.in_set(OnUpdate(GameState::InGame)))
        .add_system(camera_zoom_control.in_set(OnUpdate(GameState::InGame)))
        .add_system(sensor_system.in_set(OnUpdate(GameState::InGame)))
        .add_system(highlight_interactable_on_player_proximity.in_set(OnUpdate(GameState::InGame)))
        .add_system(
            stop_highlighting_interactable_on_player_proximity.in_set(OnUpdate(GameState::InGame)),
        )
        .add_system(sensor_system_debug.in_set(OnUpdate(GameState::InGame)))
        .add_system(test_slime_animation_with_keyboard.in_set(OnUpdate(GameState::InGame)))
        .add_system(detect_player_interaction.in_set(OnUpdate(GameState::InGame)))
        .add_system(animate_slime_on_interaction.in_set(OnUpdate(GameState::InGame)))
        .add_system(talk_to_npc.in_set(OnUpdate(GameState::InGame)))
        .add_system(player_interaction.in_set(OnUpdate(GameState::InGame)))
        .add_system(tick_and_remove_attack_cooldown.in_set(OnUpdate(GameState::InGame)))
        .add_system(player_attack_scheduler.in_set(OnUpdate(GameState::InGame)))
        .add_system(make_slime_idle_on_animation_end.in_set(OnUpdate(GameState::InGame)))
        .add_system(player_animation_reset_to_idle.in_set(OnUpdate(GameState::InGame)))
        .add_system(toggle_inventory_ui.in_set(OnUpdate(GameState::InGame)))
        // EXIT GameState::InGame
        .add_systems((pause_rapier,).in_schedule(OnExit(GameState::InGame)))
        // ENTER GameState::TalkingToNpc
        .add_system(pause_rapier.in_schedule(OnEnter(GameState::TalkingToNpc)))
        // UPDATE GameState::TalkingToNpc
        .add_systems((on_e_pressed_while_talking_to_npc,).in_set(OnUpdate(GameState::TalkingToNpc)))
        // UPDATE GameState::Menu
        .add_system(exit_menu_on_esc_pressed.in_set(OnUpdate(GameState::Menu)))
        .add_plugin(PlayerPlugin)
        .add_plugin(SlimePlugin)
        .add_system(spawn_tiles_collisions)
        .add_plugin(TmpPlugin)
        .add_system(handle_game_state_changes)
        // TODO should not be global
        .run();
}

pub fn handle_game_state_changes(state: Res<State<GameState>>) {
    if state.is_changed() {
        info!("State changed ! => {:?}", state);
    }
}

pub fn on_e_pressed_while_talking_to_npc(mut commands: Commands, keyboard: Res<Input<KeyCode>>) {
    // TODO Custom key bind
    if keyboard.just_pressed(KeyCode::E) {
        info!("E pressed while talking to NPC, switching back to ingame");
        commands.insert_resource(NextState(Some(GameState::InGame)));
    }
}
