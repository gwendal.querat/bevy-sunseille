pub const SLIME_ANIMATION_IDLE: &str = "slime_idle";
pub const SLIME_ANIMATION_RUNNING: &str = "slime_running";
pub const SLIME_ANIMATION_ATTACKING: &str = "slime_attacking";

pub const PLAYER_ANIMATION_IDLE: &str = "player_idle";
pub const PLAYER_ANIMATION_RUNNING: &str = "player_running";
pub const PLAYER_ANIMATION_ATTACKING: &str = "player_attacking";
